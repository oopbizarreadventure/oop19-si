package gui;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.ResourceBundle;
import java.util.Set;
import entity.CollisionBox;
import entity.CollisionBoxInt;
import entity.GenericProjectile;
import entity.UUIDActor;
import entity.UUIDEntity;
import entity.UUIDProjectile;
import gameengine.*;
import javafx.animation.AnimationTimer;
import javafx.event.EventHandler;
import javafx.fxml.*;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import javafx.util.Pair;
import model.WaveInfo.Difficulty;
import resourcemanager.ResourceManagerAlpha;
import virtualworld.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseEvent;

public class GameSceneController implements Initializable {
	
	private GameEngine gameEngine = new GameEnginePrototype(20 ,new GameLoggerAdaptor());
	private Stage stage = new GenericStage();
	private Image imgDrone = ResourceManagerAlpha.getIstance().getImage("drone_64x64.png");
	private Image imgTurret = ResourceManagerAlpha.getIstance().getImage("turret_64x64.png");
	private Image imgMuncher = ResourceManagerAlpha.getIstance().getImage("muncher_64x64.png");
	private Image imgBoss = ResourceManagerAlpha.getIstance().getImage("boss_1024x1024.png");
	private Image imgPlayer = ResourceManagerAlpha.getIstance().getImage("player_64x64.png");
	private Image bullet = ResourceManagerAlpha.getIstance().getImage("bullet_64x64.png");
	private Image notfound = ResourceManagerAlpha.getIstance().getImage("not_found.png");
	private Image cat = ResourceManagerAlpha.getIstance().getImage("cat_sideways.png");
	private Map<UUIDEntity, Rectangle> hitboxs = new HashMap<>();
	private AnimationTimer timer;
	
	@FXML
	private Pane pane = new Pane();
	
	@FXML
	private Label health = new Label();
	
	@FXML
	private Label ready = new Label();
	
	@FXML
	private StackPane sp = new StackPane();
	
	private Boolean isReady = false;
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
	    this.pane.setPrefHeight(ResourceManagerAlpha.getIstance().getSettingsAsObject().getHeight());
	    this.pane.setPrefWidth(ResourceManagerAlpha.getIstance().getSettingsAsObject().getWidth());
	    this.sp.setPrefHeight(ResourceManagerAlpha.getIstance().getSettingsAsObject().getHeight());
	    this.sp.setPrefWidth(ResourceManagerAlpha.getIstance().getSettingsAsObject().getWidth());
	    this.health.setText("Health: " + Integer.toString(this.stage.getplayer().getLife()));
		timer = new AnimationTimer() {
			
			@Override
			public void handle(long now) {
				if(!stage.isEnded()) {
					update();
					stage.resume();
				}else {
					gameEngine.stop();
					postRun();
				}
				
			}
		};		
		this.pane.setOnMouseClicked(e -> {
			this.pane.requestFocus();
			this.ready.setText("");
			
			if(isReady==false) {

				
				this.isReady = true;
				this.gameEngine.start(this.stage);
				while(!this.stage.isReady());

				preRun();
				timer.start();
			}
		});
	}
	
	private void preRun() {
		this.pane.setOnKeyPressed(e -> {
			if(e.getCode()==KeyCode.E) {
				this.shoot();
			}
		});
		
		this.pane.setOnMouseMoved(new EventHandler<MouseEvent>(){

			@Override
			public void handle(MouseEvent event) {
				stage.getplayer().move((int)event.getSceneX(), stage.getplayer().getBody().getCollisionBox().getY());
			}
		});
	}
	
	private void update() {
		this.health.setText("Health: " + Integer.toString(this.stage.getplayer().getLife()));
		this.stage.resume();
		this.updateHitbox();
	}
	
	private void postRun() {
		if(MainMenuController.getUsername()!=null) {
			ResourceManagerAlpha.getIstance().getLeaderboardAsObject().addRecord(new Pair<>(MainMenuController.getUsername().get(), this.stage.getScore()));
			ResourceManagerAlpha.getIstance().saveLeaderboard(ResourceManagerAlpha.getIstance().getLeaderboardAsObject());
		}
		timer.stop();
		javafx.stage.Stage stage = (javafx.stage.Stage) pane.getScene().getWindow();
		if(ControlsMenuController.getDiff()==Difficulty.HARD) {
			ImageView imgView = new ImageView(this.cat);
			imgView.setLayoutX(ResourceManagerAlpha.getIstance().getSettingsAsObject().getWidth()/2);
			imgView.setLayoutY(ResourceManagerAlpha.getIstance().getSettingsAsObject().getHeight()/2);
			this.pane.getChildren().add(imgView);
		}
		Utilities.load("MainMenu.fxml", stage);
	}
	
	public void run() {
		preRun();
		update();
		postRun();	
	}
	
	public void shoot() {
		this.stage.getplayer().fire();
	}
	
	private Image selectImage(UUIDEntity a) {
            switch (a.getType()) {
                case "Drone":
                    return this.imgDrone;

                case "Turret":
                    return this.imgTurret;

                case "Muncher":
                    return this.imgMuncher;

                case "Boss":
                    return this.imgBoss;

                case "Player":
                    return this.imgPlayer;
                case "Generic-Projectile":
                    return this.bullet;

                default:
                    return this.notfound;
            }
    }
	
	
	private Rectangle load(UUIDEntity a) {
            CollisionBox<Integer> box = new CollisionBoxInt(a.getBody().getCollisionBox());
            Rectangle rect = new Rectangle(box.getWidth(),box.getHeight());
            rect.setFill(new ImagePattern(selectImage(a)));
            rect.setVisible(true);
            return rect;
    }
	
	
	private void updateHitbox() {
		//removeDeads
        Set<UUIDEntity> deads = Set.copyOf(hitboxs.keySet());
        synchronized (hitboxs) {
            for (UUIDEntity dead : deads) {
                    this.pane.getChildren().remove(this.hitboxs.remove(dead));
                }
        }
        
        //addNewSpawned         
        for (Entry<UUIDActor, CollisionBox<Integer>> entry : this.stage.getMap().getActors().entrySet()) {
            if (!this.hitboxs.containsKey(entry.getKey())) {
                Rectangle imgView = load((UUIDEntity)entry.getKey());
                imgView.setWidth(imgView.getWidth()*(ResourceManagerAlpha.getIstance().getSettingsAsObject().getWidth()/860));
                imgView.setHeight(imgView.getHeight()*(ResourceManagerAlpha.getIstance().getSettingsAsObject().getHeight()/540));
                this.pane.getChildren().add(imgView);
                this.hitboxs.put(entry.getKey(), imgView);                  
            }
        }
        for (Entry<UUIDProjectile, CollisionBox<Integer>> entry : this.stage.getMap().getProjectiles().entrySet()) {
            if (!this.hitboxs.containsKey(entry.getKey())) {
            	Rectangle imgView = load((UUIDEntity)entry.getKey());
                this.pane.getChildren().add(imgView);
                this.hitboxs.put(entry.getKey(), imgView);                  
            }
        }
        //updatePositions
        for (Entry<UUIDEntity,Rectangle> entry: this.hitboxs.entrySet()) {
            entry.getValue().relocate(entry.getKey().getBody().getCollisionBox().getX(),
                    stage.getMap().getHeigth()-entry.getKey().getBody().getCollisionBox().getY() + (entry.getKey().getType().equals(GenericProjectile.TYPE)? 32 : 0));
        }
	}
}
