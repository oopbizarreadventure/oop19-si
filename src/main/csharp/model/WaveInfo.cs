﻿using System.Collections.Generic;

namespace model
{
	/// <summary>
	/// Interface for the creation of game-play waves,
	/// where the player and enemies are actors.
	/// </summary>
	public interface WaveInfo
	{

		/// <summary>
		/// Returns an elaborate HP value based on parameters.
		/// </summary>
		/// <param name="baseValue"> </param>
		/// <returns> Enemy HP </returns>
		int elaborateInitialHP(int baseValue);

		/// <summary>
		/// Returns an elaborate DAMAGE value based on parameters.
		/// </summary>
		/// <param name="baseValue"> </param>
		/// <returns> Enemy DAMAGE </returns>
		int elaborateInitialDMG(int baseValue);

		/// <summary>
		/// Returns an elaborate Score Points Amount based on parameters.
		/// </summary>
		/// <param name="enemyTier"> </param>
		/// <returns> Score </returns>
		int elaborateScore(int enemyTier);

		WaveInfo_Difficulty Difficulty {get;}

		int Number {get;}

		/// <summary>
		/// Every WaveInfo will have a specific difficulty,
		/// with some specific starting parameters.
		/// </summary>
		/// <returns> Enemy Level, Enemy Damage Multiplier, Enemy Saturation, Enemy Spawn-cap </returns>

	}

	public sealed class WaveInfo_Difficulty
	{
		public static readonly WaveInfo_Difficulty EASY = new WaveInfo_Difficulty("EASY", InnerEnum.EASY, 1, 1, 10);
		public static readonly WaveInfo_Difficulty NORMAL = new WaveInfo_Difficulty("NORMAL", InnerEnum.NORMAL, 5, 2, 15);
		public static readonly WaveInfo_Difficulty HARD = new WaveInfo_Difficulty("HARD", InnerEnum.HARD, 10, 3, 20);

		private static readonly List<WaveInfo_Difficulty> valueList = new List<WaveInfo_Difficulty>();

		static WaveInfo_Difficulty()
		{
			valueList.Add(EASY);
			valueList.Add(NORMAL);
			valueList.Add(HARD);
		}

		public enum InnerEnum
		{
			EASY,
			NORMAL,
			HARD
		}

		public readonly InnerEnum innerEnumValue;
		private readonly string nameValue;
		private readonly int ordinalValue;
		private static int nextOrdinal = 0;

		internal int enemyLVL;
		internal int enemyDMGmulti;
		internal int enemySATUR;

//JAVA TO C# CONVERTER WARNING: 'final' parameters are ignored unless the option to convert to C# 7.2 'in' parameters is selected:
//ORIGINAL LINE: WaveInfo_Difficulty(final int enemyLVL, final int enemyDMGmulti, final int enemySPWNcap)
		internal WaveInfo_Difficulty(string name, InnerEnum innerEnum, int enemyLVL, int enemyDMGmulti, int enemySPWNcap)
		{
			this.enemyLVL = enemyLVL;
			this.enemyDMGmulti = enemyDMGmulti;
			this.enemySATUR = enemySPWNcap;

			nameValue = name;
			ordinalValue = nextOrdinal++;
			innerEnumValue = innerEnum;
		}

		public int EnemyLVL
		{
			get
			{
				return enemyLVL;
			}
		}
		public int EnemyDMGmulti
		{
			get
			{
				return enemyDMGmulti;
			}
		}
		public int EnemySATUR
		{
			get
			{
				return enemySATUR;
			}
		}
		public int EnemySPWNcap
		{
			get
			{
				return enemySATUR * enemyDMGmulti;
			}
		}

		public static WaveInfo_Difficulty[] values()
		{
			return valueList.ToArray();
		}

		public int ordinal()
		{
			return ordinalValue;
		}

		public override string ToString()
		{
			return nameValue;
		}

		public static WaveInfo_Difficulty valueOf(string name)
		{
			foreach (WaveInfo_Difficulty enumInstance in WaveInfo_Difficulty.valueList)
			{
				if (enumInstance.nameValue == name)
				{
					return enumInstance;
				}
			}
			throw new System.ArgumentException(name);
		}
	}

}