﻿namespace model
{

	/// <summary>
	/// Implementation of the Wave Generator.
	/// </summary>
	public class WaveInfoGeneratorImpl : WaveInfoGenerator
	{

		private readonly WaveInfo_Difficulty waveDiff;

//JAVA TO C# CONVERTER WARNING: 'final' parameters are ignored unless the option to convert to C# 7.2 'in' parameters is selected:
//ORIGINAL LINE: public WaveInfoGeneratorImpl(final model.WaveInfo_Difficulty waveDiff)
		public WaveInfoGeneratorImpl(WaveInfo_Difficulty waveDiff)
		{
			this.waveDiff = waveDiff;
		}

//JAVA TO C# CONVERTER WARNING: 'final' parameters are ignored unless the option to convert to C# 7.2 'in' parameters is selected:
//ORIGINAL LINE: @Override public final WaveInfo loadStageStandard(final int stageNUM)
		public WaveInfo loadStageStandard(int stageNUM)
		{
			return new WaveInfoAnonymousInnerClass(this, stageNUM);
		}

		private class WaveInfoAnonymousInnerClass : WaveInfo
		{
			private readonly WaveInfoGeneratorImpl outerInstance;

			private int stageNUM;

			public WaveInfoAnonymousInnerClass(WaveInfoGeneratorImpl outerInstance, int stageNUM)
			{
				this.outerInstance = outerInstance;
				this.stageNUM = stageNUM;
			}


//JAVA TO C# CONVERTER WARNING: 'final' parameters are ignored unless the option to convert to C# 7.2 'in' parameters is selected:
//ORIGINAL LINE: public int elaborateInitialHP(final int baseValue)
			public int elaborateInitialHP(int baseValue)
			{
				return (int) baseValue * (outerInstance.waveDiff.EnemyLVL + stageNUM);
			}

//JAVA TO C# CONVERTER WARNING: 'final' parameters are ignored unless the option to convert to C# 7.2 'in' parameters is selected:
//ORIGINAL LINE: public int elaborateInitialDMG(final int baseValue)
			public int elaborateInitialDMG(int baseValue)
			{
				return (int) baseValue * outerInstance.waveDiff.EnemyDMGmulti + stageNUM;
			}

//JAVA TO C# CONVERTER WARNING: 'final' parameters are ignored unless the option to convert to C# 7.2 'in' parameters is selected:
//ORIGINAL LINE: public int elaborateScore(final int enemyTier)
			public int elaborateScore(int enemyTier)
			{
				return (int) enemyTier * outerInstance.waveDiff.EnemyDMGmulti;
			}

			public WaveInfo_Difficulty Difficulty
			{
				get
				{
					return outerInstance.waveDiff;
				}
			}

			public int Number
			{
				get
				{
					return stageNUM;
				}
			}
		}

		public WaveInfo_Difficulty Difficulty
		{
			get
			{
				return this.waveDiff;
			}
		}

	}

}