﻿namespace model
{

	/// <summary>
	/// This Class provides a data structure for storing
	/// information about a Level and its waves.
	/// 
	/// Axel Arfelli
	/// </summary>
	public interface LevelData
	{

		WaveInfo_Difficulty LevelDifficulty {get;}

		int TotalWaves {get;}

		int LastWaveNumber {get;set;}


		void increaseLastWaveNumber();

		int TotalScore {get;set;}


		bool Endless {get;}

	}

}